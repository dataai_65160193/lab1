import java.util.Arrays;

public class lab12 {
    public static void duplicateZeros(int[] arr) {
        int length = arr.length;
        int zeroCount = 0;

        // Count the number of zeros in the original array
        for (int num : arr) {
            if (num == 0) {
                zeroCount++;
            }
        }

        // Calculate the new length after duplicating zeros
        int newLength = length + zeroCount;

        // Start from the end of the original array
        for (int i = length - 1, j = newLength - 1; i >= 0; i--, j--) {
            if (arr[i] == 0) {
                // If the current element is zero, duplicate it and decrement j
                if (j < length) {
                    arr[j] = 0;
                }
                j--;

                // Copy the original zero one more time if there's space
                if (j < length) {
                    arr[j] = 0;
                }
            } else {
                // Copy non-zero elements to their new positions
                if (j < length) {
                    arr[j] = arr[i];
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 0, 2, 3, 0, 4, 5, 0};
        duplicateZeros(arr1);
        System.out.println("Output for Example 1: " + Arrays.toString(arr1));

        int[] arr2 = {1, 2, 3};
        duplicateZeros(arr2);
        System.out.println("Output for Example 2: " + Arrays.toString(arr2));
    }
}

