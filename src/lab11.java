import java.util.Arrays;

public class lab11 {
    public static void main(String[] args) {
        // Declare and initialize arrays
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];

        // Print elements of the "numbers" array using a for loop
        System.out.println("Elements of the 'numbers' array:");
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        // Print elements of the "names" array using a for-each loop
        System.out.println("\nElements of the 'names' array:");
        for (String name : names) {
            System.out.println(name);
        }

        // Initialize elements of the "values" array with decimal values
        values[0] = 1.5;
        values[1] = 2.7;
        values[2] = 3.2;
        values[3] = 4.8;

        // Calculate and print the sum of elements in the "numbers" array
        int sum = 0;
        for (int num : numbers) {
            sum += num;
        }
        System.out.println("\nSum of elements in the 'numbers' array: " + sum);

        // Find and print the maximum value in the "values" array
        double max = values[0];
        for (int i = 1; i < values.length; i++) {
            if (values[i] > max) {
                max = values[i];
            }
        }
        System.out.println("\nMaximum value in the 'values' array: " + max);

        // Create a new string array "reversedNames" and fill it with reversed elements from "names"
        String[] reversedNames = new String[names.length];
        for (int i = 0, j = names.length - 1; i < names.length; i++, j--) {
            reversedNames[i] = names[j];
        }

        // Print the elements of the "reversedNames" array
        System.out.println("\nElements of the 'reversedNames' array:");
        for (String reversedName : reversedNames) {
            System.out.println(reversedName);
        }

        // Bonus: Sort the "numbers" array in ascending order
        Arrays.sort(numbers);

        // Print the sorted "numbers" array
        System.out.println("\nSorted 'numbers' array:");
        for (int num : numbers) {
            System.out.println(num);
        }
    }
}
